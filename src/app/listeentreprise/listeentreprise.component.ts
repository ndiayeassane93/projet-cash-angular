import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-listeentreprise',
  templateUrl: './listeentreprise.component.html',
  styleUrls: ['./listeentreprise.component.css']
})
export class ListeentrepriseComponent implements OnInit {
dataEntre: Object;
  constructor(private dataService: DataService) { }

  ngOnInit() {
      this.dataService.getEntreprises().subscribe(dataService => this.dataEntre = dataService);
  }
  deleteEntre(id) {
    this.dataService.deleteEntreprise(id).subscribe(_ => console.log('succes'));
  }

}
