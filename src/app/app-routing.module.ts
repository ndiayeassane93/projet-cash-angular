import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { ListecitoyensComponent } from './listecitoyens/listecitoyens.component';
import { ListeentrepriseComponent } from './listeentreprise/listeentreprise.component';
import { FormulairecitoyenComponent } from './formulairecitoyen/formulairecitoyen.component';
import { FormulaireentrepriseComponent } from './formulaireentreprise/formulaireentreprise.component';
import { EditComponent } from './edit/edit.component';



const routes: Routes = [
    {path: 'listeCitoyens', component: ListecitoyensComponent},
    {path: 'edit/:id', component: EditComponent},
    {path: 'formulaireCitoyen', component: FormulairecitoyenComponent},
    {path: 'formulaireEntreprise' , component: FormulaireentrepriseComponent},
    {path: 'listeEntreprises', component: ListeentrepriseComponent},
    { path: '', redirectTo: 'accueil', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
