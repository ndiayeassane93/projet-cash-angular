import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient ) {}


  getCitoyens(): Observable<Object> {
   return this.http.get<Object>('http://localhost:8080/projet-cash-rest/citoyens/liste');

  }
  getCitoyen(id): Observable<Object> {
      return this.http.get<Object>('http://localhost:8080/projet-cash-rest/citoyens/' + id);

  }
  getEntreprises(): Observable<Object> {
      return this.http.get<Object>('http://localhost:8080/projet-cash-rest/entreprise/liste');
  }
  createCitoyens(citoyen): Observable<Object> {
      const httpOptions = {
        headers: new HttpHeaders ({'content-type': 'application/json'})
    };
    return this.http.post('http://localhost:8080/projet-cash-rest/citoyens/ajouter', citoyen, httpOptions);
  }
    deleteCitoyen(id): Observable<Object> {
        const httpOptions = {
        headers: new HttpHeaders ({'content-type': 'application/json'})
    };
      return this.http.delete<Object>('http://localhost:8080/projet-cash-rest/citoyens/supprimer/' + id, httpOptions);
  }

  /* editCitoyen(citoyen): Observable<Object> {
      const id = +this.route.snapshot.params['id'];
        const httpOptions = {
        headers: new HttpHeaders ({'content-type': 'application/json'})
    };
      return this.http.put('http://localhost:8080/projet-cash-rest/citoyens/modifier/' + id , citoyen, httpOptions);

  }*/


  createEntreprise(entreprise): Observable<Object> {
      const httpOptions = {
        headers: new HttpHeaders ({'content-type': 'application/json'})
    };
    return this.http.post('http://localhost:8080/projet-cash-rest/entreprise/ajouter', entreprise, httpOptions);
  }


  deleteEntreprise(id): Observable<Object> {
        const httpOptions = {
        headers: new HttpHeaders ({'content-type': 'application/json'})
    };
      return this.http.delete('http://localhost:8080/projet-cash-rest/entreprise/supprimer/' + id, httpOptions);
  }

}
