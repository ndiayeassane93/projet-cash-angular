import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireentrepriseComponent } from './formulaireentreprise.component';

describe('FormulaireentrepriseComponent', () => {
  let component: FormulaireentrepriseComponent;
  let fixture: ComponentFixture<FormulaireentrepriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulaireentrepriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
