import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaireentreprise',
  templateUrl: './formulaireentreprise.component.html',
  styleUrls: ['./formulaireentreprise.component.css']
})
export class FormulaireentrepriseComponent implements OnInit {
    entreprises: Object = {};
  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
  }
  createEntreprise() {
     this.dataService.createEntreprise(this.entreprises).subscribe(dataService => this.entreprises = dataService);
     this.router.navigate(['/listeEntreprises']);
  }

}
