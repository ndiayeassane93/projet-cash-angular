import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { ListecitoyensComponent } from './listecitoyens/listecitoyens.component';
import { DataService } from './data.service';
import { AppRoutingModule } from './app-routing.module';
import { ListeentrepriseComponent } from './listeentreprise/listeentreprise.component';
import { AccueilComponent } from './accueil/accueil.component';
import { FormulairecitoyenComponent } from './formulairecitoyen/formulairecitoyen.component';
import { FormulaireentrepriseComponent } from './formulaireentreprise/formulaireentreprise.component';
import { EditComponent } from './edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    ListecitoyensComponent,
    ListeentrepriseComponent,
    AccueilComponent,
    FormulairecitoyenComponent,
    FormulaireentrepriseComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
