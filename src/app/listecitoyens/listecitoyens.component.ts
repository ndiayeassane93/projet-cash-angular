import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router} from '@angular/router';



@Component({
  selector: 'app-listecitoyens',
  templateUrl: './listecitoyens.component.html',
  styleUrls: ['./listecitoyens.component.css']
})
export class ListecitoyensComponent implements OnInit {

    data: Object = [];


  constructor(private router: Router, private dataService: DataService) { }


  ngOnInit() {

    this.dataService.getCitoyens().subscribe(dataService => this.data = dataService);
  }
  goBack(): void {
    this.router.navigate(['/listeCitoyens']);
  }

  deleteCitoyens(id: any) {
      this.dataService.deleteCitoyen(id).subscribe( _ => console.log('succes'));
      this.goBack();
  }




  }


