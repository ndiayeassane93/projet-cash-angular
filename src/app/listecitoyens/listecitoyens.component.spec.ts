import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListecitoyensComponent } from './listecitoyens.component';

describe('ListecitoyensComponent', () => {
  let component: ListecitoyensComponent;
  let fixture: ComponentFixture<ListecitoyensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListecitoyensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListecitoyensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
