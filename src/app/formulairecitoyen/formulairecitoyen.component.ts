import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulairecitoyen',
  templateUrl: './formulairecitoyen.component.html',
  styleUrls: ['./formulairecitoyen.component.css']
})
export class FormulairecitoyenComponent implements OnInit {

    citoyen: Object = { };

  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
  }
  goBack(): void {
    this.router.navigate(['/listeCitoyens']);
  }
  createCitoyen() {

       this.dataService.createCitoyens(this.citoyen)
              .subscribe(dataService => this.citoyen = dataService);
              this.router.navigate(['/listeCitoyens']);

  }

}
