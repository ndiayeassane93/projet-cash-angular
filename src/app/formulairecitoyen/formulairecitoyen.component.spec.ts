import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulairecitoyenComponent } from './formulairecitoyen.component';

describe('FormulairecitoyenComponent', () => {
  let component: FormulairecitoyenComponent;
  let fixture: ComponentFixture<FormulairecitoyenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormulairecitoyenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulairecitoyenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
